﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net.Http;
using System.Net.Http.Headers;
using DotNetNuke.Web.Api;
using System.Web.Http;
using System.Net;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Pervasive.Data.Common;
using Pervasive.Data.SqlClient;
using System.Data;
using System.IO;
using DotNetNuke.Services.Exceptions;

using Coesolutions.Modules.TampaPilotsDataService.Models;

namespace Coesolutions.Modules.TampaPilotsDataService.Services
{
    public class MainController : ControllerBase
    {


        #region "Data Calls"



        /// <summary>
        /// Very first test of public method.   Use  to test connectivity
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            var response = new HttpResponseMessage();
            response.Content = new StringContent("<html><body>Hello World</body></html>");
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }

        /// <summary>
        /// IOS cannot download PDF directly.   This is called as tde proxuURL by Kendo UI
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [AllowAnonymous]
        // POST api/savefile
        public HttpResponseMessage Post([FromBody]FileData file)
        {
            var data = Convert.FromBase64String(file.Base64);

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(new MemoryStream(data))
            };

            result.Content.Headers.ContentType = new MediaTypeHeaderValue(file.ContentType);

            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = file.FileName
            };

            return result;
        }



        [AllowAnonymous]
        [HttpGet]
        public List<PublicRundown> RundownsForPublic(string direction)
        {
            return sqlRundownsForPublic(direction);
        }


        #endregion

        #region "SQL Calls"

        protected PsqlConnection conn()
        {
            PsqlConnection con = new PsqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings

                ["TampaConnectionString"].ConnectionString);

            return con;

        }


        protected List<PublicRundown> sqlRundownsForPublic(string direction)
        {

            List<PublicRundown> intList = new List<PublicRundown>();

            PsqlCommand cmd = new PsqlCommand(qryPublicRundowns(direction), conn());

            cmd.Connection.Open();

            
            PsqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    PublicRundown pr = new PublicRundown();


                    try
                    {
                        pr.VesselName = reader["Vessel_Name"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        pr.VesselName = "ERROR";
                    }

                    //Job Start
                    try
                    {
                        pr.JobStartDateTime = Convert.ToDateTime(reader["JobStartDate"]) +
                            Convert.ToDateTime(reader["JobStartTime"]).TimeOfDay;
                    }
                    catch (Exception)
                    {

                        pr.JobStartDateTime = new DateTime(1900, 1, 1);
                    }
                    //All Fast
                    try
                    {
                        pr.AllFastDateTime = Convert.ToDateTime(reader["AllFastDate"]) +
                            Convert.ToDateTime(reader["AllFastTime"]).TimeOfDay;
                    }
                    catch (Exception)
                    {

                        pr.AllFastDateTime = new DateTime(1900, 1, 1);
                    }

                    //Sea Buoy
                    try
                    {
                        pr.SeaBuoyDateTime = Convert.ToDateTime(reader["SeabouyDate"]) +
                            Convert.ToDateTime(reader["SeabouyTime"]).TimeOfDay;
                    }
                    catch (Exception)
                    {

                        pr.SeaBuoyDateTime = new DateTime(1900, 1, 1);
                    }


                    //Light
                    try
                    {
                        pr.LightDateTime = Convert.ToDateTime(reader["LightDate"]) +
                            Convert.ToDateTime(reader["LightTime"]).TimeOfDay;
                    }
                    catch (Exception)
                    {

                        pr.LightDateTime = new DateTime(1900, 1, 1);
                    }


                    //AlongSide
                    try
                    {
                        pr.AlongSideDateTime = Convert.ToDateTime(reader["AlongSideDate"]) +
                            Convert.ToDateTime(reader["AlongSideTime"]).TimeOfDay;
                    }
                    catch (Exception)
                    {

                        pr.AlongSideDateTime = new DateTime(1900, 1, 1);
                    }

                    int dFeet = 0;
                    try
                    {
                        dFeet = Convert.ToInt32(reader["Draft_Footage"]);
                    }
                    catch (Exception)
                    {

                        dFeet = 0;
                    }

                    int dInch = 0;

                    try
                    {
                        dInch = Convert.ToInt32(reader["Draft_Inches"]);
                    }
                    catch (Exception)
                    {

                        dInch = 0;
                    }

                    try
                    {
                        pr.DraftString = dFeet.ToString() + "'" + dInch.ToString() + @"""";
                    }
                    catch (Exception)
                    {

                        pr.DraftString = "ERROR";
                    }

                    try
                    {
                        pr.Speed = Convert.ToDecimal(reader["SpeedKTS"]);
                    }
                    catch (Exception)
                    {

                        pr.Speed = 0;
                    }

                    try
                    {
                        pr.FromDesc = reader["FromDesc"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        pr.FromDesc = "";
                    }
                    try
                    {
                        pr.ToDesc = reader["ToDesc"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        pr.ToDesc = "";
                    }

                    try
                    {
                        pr.TugCompanyFrom = reader["TugCompanyFrom"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        pr.TugCompanyFrom = "";
                    }

                    try
                    {
                        pr.TugPrimary = reader["TugPrimary"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        pr.TugPrimary = "";
                    }

                    try
                    {
                        pr.AgentName = reader["AgentName"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        pr.AgentName = "";
                    }


                    try
                    {
                        pr.AgentPhone = reader["AgentPhone"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        pr.AgentPhone = "";
                    }

                    try
                    {
                        pr.VesselFlag = reader["VesselFlag"].ToString().Trim();
                    }
                    catch (Exception)
                    {

                        pr.VesselFlag = "";
                    }


                    try
                    {
                        pr.Notes = reader["Notes"].ToString().TrimEnd();
                        pr.Notes = pr.Notes.Replace("\r", "<br>");
                        pr.Notes = pr.Notes.Replace("\n", "<br>");
                    }
                    catch (Exception)
                    {

                        pr.Notes = "";
                    }

                    try
                    {
                        if (reader["SafetyZone_YN"].ToString().Contains("Y"))
                        {

                            pr.SafetyZone = "YES";
                        }
                        else
                        {
                            pr.SafetyZone = "";

                        }
                    }
                    catch (Exception)
                    {

                        pr.SafetyZone = "";
                    }








                    intList.Add(pr);
                }


            }
            else
            {
                

            }




            return intList;

        }



        #endregion


        #region "SQL Queries"

        protected string qryPublicRundowns(string direction)
        {

            string intString = @"SELECT rt.Vessel_Name 
                        ,rt.JobStartDate
                        ,rt.JobStartTime
                        ,rt.AllFastDate
                        ,rt.AllFastTime
                        ,rt.SeabouyDate
                        ,rt.SeabouyTime
                        ,rt.LightDate
                        ,rt.LightTime
                        ,rt.AlongSideDate
                        ,rt.AlongSideTime
                        ,rt.Draft_Footage
                        ,rt.Draft_Inches
                        ,rt.SpeedKTS
                        ,mf.Description1 AS FromDesc
                        ,mt.Description1 AS ToDesc
                        ,rt.Tug_ID_From as TugCompanyFrom
                        ,rt.Tug_Primary as TugPrimary
                        ,a.AgentName
                        ,a.Phone AS AgentPhone
                        ,rt.SafetyZone_YN 
                        ,v.Flag as VesselFlag
                        ,rt.Notes
                        from Rundown_Table rt
                        LEFT JOIN Vessels v
                        ON rt.Call_Sign = v.CallSign
                        LEFT JOIN Mileage_Codes mf
                        ON rt.From_Mileage_Code = mf.MileCode
                        LEFT JOIN Mileage_Codes mt
                        ON rt.To_Mileage_Code = mt.MileCode
                        LEFT JOIN Agent_Codes a
                        ON rt.Order_Agent_Code = a.AgentCode ";


            switch  (direction)
            {

                case  "inbound":
                   intString += @" WHERE Turn_Status_Pending <> 'C'
                    AND Vessel_Name <> ''
                    AND From_Mileage_Code = 'S' ";

                   break;

                case "outboundandshifts":

                    intString += @" WHERE Turn_Status_Pending <> 'C'
                        AND Vessel_Name <> ''
                        AND To_Mileage_Code = 'S'
                        OR (
                        To_Mileage_Code <> 'S' 
                        AND From_Mileage_Code <> 'S' 
                        AND Vessel_Name <> ''
                        AND Turn_Status_Pending <> 'C'
                        )

                        ORDER BY Est__Date_On_Board
                        ,Time_Estimate";

                    break;

            }

            intString += @" ORDER BY Est__Date_On_Board
                    ,Time_Estimate ";
              




            return intString;

        }



        #endregion

    }
}
