﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotNetNuke.Web.Api;

namespace Coesolutions.Modules.TampaPilotsDataService.Services
{
    public class RouteMapper : IServiceRouteMapper
    {
        public void RegisterRoutes(IMapRoute mapRouteManager)
        {
            mapRouteManager.MapHttpRoute(
                "TampaPilotsDataService",
                "default",
                "{controller}/{action}",
                new string[] { "Coesolutions.Modules.TampaPilotsDataService.Services" });
        }
    }
}
