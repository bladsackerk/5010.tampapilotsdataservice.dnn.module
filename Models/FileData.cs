﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coesolutions.Modules.TampaPilotsDataService.Models
{


    /// <summary>
    /// Used by the Post Method to create HTML contrent to return for IOS needs
    /// </summary>
    public class FileData
    {
        public string ContentType { get; set; }
        public string Base64 { get; set; }
        public string FileName { get; set; }
    }

}
