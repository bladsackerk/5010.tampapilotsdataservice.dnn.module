﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coesolutions.Modules.TampaPilotsDataService.Models
{
    public class PublicRundown
    {
        public string VesselName { get; set; }
        public DateTime JobStartDateTime { get; set; }
        public DateTime AllFastDateTime { get; set; }
        public DateTime PilotOrderedDateTime { get; set; }
        public DateTime SeaBuoyDateTime { get; set; }
        public DateTime LightDateTime { get; set; }
        public DateTime AlongSideDateTime { get; set; }
        public String DraftString { get; set; }
        public decimal Speed { get; set; }
        public string FromDesc { get; set; }
        public string ToDesc { get; set; }
        public string TugCompanyFrom { get; set; }
        public string TugPrimary { get; set; }
        public string AgentName { get; set; }
        public string AgentPhone { get; set; }
        public string SafetyZone { get; set; }
        public string VesselFlag { get; set; }
        public string Notes { get; set; }


    }
}
